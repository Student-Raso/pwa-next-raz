"use client";
import { useState } from 'react'

const HomePage = () => {

  const [ file, setFile ] = useState(null);

  return (
    <div>
      <form
        onSubmit={ async (e) => {

          e.preventDefault();

          const formData = new FormData();
          formData.append('file', file);

          const response = await fetch("/api/upload", {
            method: "POST",
            body: formData
          });
          const data = await response?.json();
          console.log('data en page.js')
          console.log(data)
        }}
      >
        <input 
          type="file" 
          onChange={ e => 
            setFile(e.target.files[0])
          }
        /><br/>
        <button>
          SOMETER
        </button>
      </form>
    </div>
  )
}

export default HomePage